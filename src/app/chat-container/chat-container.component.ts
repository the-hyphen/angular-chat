import { Component, OnInit, ViewChild } from '@angular/core';
import { ChatInputComponent } from '../chat-input/chat-input.component';
import { Message } from '../message';
import { MessageService } from '../message.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-chat-container',
  templateUrl: './chat-container.component.html',
  styleUrls: ['./chat-container.component.scss']
})
export class ChatContainerComponent {
  receiver: string = '';
  messages: Observable<Message[]> = new Observable();

  @ViewChild(ChatInputComponent) chatInput: ChatInputComponent;

  private currentText = '';

  constructor(private messageService: MessageService) {
    this.messages = messageService.messages$;
  }

  chatInputChange(text) {
    this.currentText = text;
  }

  sendMessage() {
    // send
    this.messageService.send(this.currentText);
    this.chatInput.reset();
    this.messageService.waitForNewMessage(3000);
  }

}
