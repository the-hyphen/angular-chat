import { Injectable } from "@angular/core";
import { Observable, Subject } from "rxjs";
import { Message } from "./message";

@Injectable({
  providedIn: "root"
})
export class MessageService {
  private messages: Message[] = [];
  private messages_: Subject<Message[]> = new Subject();
  messages$: Observable<Message[]> = this.messages_.asObservable();

  addMessage(message: Message) {
    this.messages.push(message);
    this.messages_.next(this.messages);
  }

  send(text: string) {
    this.addMessage({ text, received: false });
  }

  receive(text: string) {
    this.addMessage({ text, received: true });
  }

  waitForNewMessage(delay = 2000) {
    setTimeout(() => {
      this.receive("New Message from Friend");
    }, delay);
  }
}
