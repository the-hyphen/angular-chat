export interface Message {
  text: string;
  received: boolean;
}
