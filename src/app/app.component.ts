import { Component, ViewChild } from '@angular/core';
import { ChatInputComponent } from './chat-input/chat-input.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  @ViewChild(ChatInputComponent) chatInput: ChatInputComponent;
  title = 'angular-chat';

  reset() {
    this.chatInput.reset();
  }

  buttonClick(change) {
    console.log(change);
  }
}
