import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-send-button',
  templateUrl: './send-button.component.html',
  styleUrls: ['./send-button.component.scss']
})
export class SendButtonComponent {
  @Output() onClick: EventEmitter<any> = new EventEmitter();
  constructor() { }

  handleClick() {
    this.onClick.emit();
  }

}
