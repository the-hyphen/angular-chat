import {
  Component,
  OnInit,
  Output,
  EventEmitter,
  ViewChild
} from "@angular/core";

@Component({
  selector: "app-chat-input",
  templateUrl: "./chat-input.component.html",
  styleUrls: ["./chat-input.component.scss"]
})
export class ChatInputComponent {
  @Output() onChange: EventEmitter<string> = new EventEmitter();
  chatInput: string;

  constructor() {}

  reset() {
    this.chatInput = '';
  }

  handleChange() {
    this.onChange.emit(this.chatInput);
  }
}
